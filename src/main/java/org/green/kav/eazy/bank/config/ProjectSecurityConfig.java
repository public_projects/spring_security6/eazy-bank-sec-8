package org.green.kav.eazy.bank.config;

import org.green.kav.eazy.bank.filter.AuthoritiesLoggingAfterFilter;
import org.green.kav.eazy.bank.filter.CsrfCookieFilter;
import org.green.kav.eazy.bank.filter.LoggingFilter;
import org.green.kav.eazy.bank.filter.RequestValidationBeforeFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.cors.CorsConfiguration;

import javax.sql.DataSource;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Configuration
public class ProjectSecurityConfig {
    public final static String ACCOUNT_URL = "/myAccount";
    public final static String BALANCE_URL = "/myBalance";
    public final static String LOAN_URL = "/myLoans";
    public final static String CARD_URL = "/myCards";
    public final static String NOTICE_URL = "/notices";
    public final static String CONTACT_URL = "/contact";



    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        CsrfTokenRequestAttributeHandler requestAttributeHandler = new CsrfTokenRequestAttributeHandler();
        requestAttributeHandler.setCsrfRequestAttributeName("_csrf");

        http.securityContext(httpSecuritySecurityContextConfigurer -> {
            httpSecuritySecurityContextConfigurer.requireExplicitSave(false);
        }).sessionManagement(httpSecuritySessionManagementConfigurer -> {
            httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
        });

        // CORS (58. Fixing CORs issue using spring security)
        http.cors(httpSecurityCorsConfigurer -> httpSecurityCorsConfigurer.configurationSource(request -> {
                    CorsConfiguration configuration = new CorsConfiguration();
                    configuration.setAllowedOrigins(
                            List.of("http://localhost:4200")
                    );
                    configuration.setAllowedMethods(List.of(
                            RequestMethod.GET.name(),
                            RequestMethod.POST.name()));
                    configuration.setAllowCredentials(true);
                    configuration.setAllowedHeaders(List.of("*"));
                    configuration.setMaxAge(Duration.of(1L, ChronoUnit.HOURS)); // Browser can remember configuration for 1 hours.
                    return configuration;
                })
        );

        // example of configuring the CSRF (62. Ignoring CSRF protection for public APIs)
        http.csrf(csrf -> {
            csrf.ignoringRequestMatchers("/register", CONTACT_URL)
                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                    .csrfTokenRequestHandler(requestAttributeHandler);
        });

        // 78. addFilter after example
        http.addFilterAfter(new AuthoritiesLoggingAfterFilter(), BasicAuthenticationFilter.class);

        // 79. addFilterAt changes.
        http.addFilterAt(new LoggingFilter(), BasicAuthenticationFilter.class);

        // 63. Implementing CSRF token solution inside our web application
        http.addFilterAfter(new CsrfCookieFilter(), BasicAuthenticationFilter.class);

        // 77. Adding custom filter
        http.addFilterBefore(new RequestValidationBeforeFilter(), BasicAuthenticationFilter.class);

        // 70. Use of Authorities
//        http.authorizeHttpRequests((requests) -> {
//            requests.requestMatchers(ACCOUNT_URL).hasAuthority("VIEWACCOUNT");
//            requests.requestMatchers(LOAN_URL).hasAuthority("VIEWLOANS");
//            requests.requestMatchers(CARD_URL).hasAuthority("VIEWCARDS");
//            requests.requestMatchers(BALANCE_URL).hasAnyAuthority("VIEWACCOUNT", "VIEWBALANCE");
//        });

        // 73. Use or Role
        http.authorizeHttpRequests((requests) -> {
            requests.requestMatchers(ACCOUNT_URL).hasRole("USER");
            requests.requestMatchers(LOAN_URL).hasRole("USER");
            requests.requestMatchers(CARD_URL).hasRole("USER");
            requests.requestMatchers(BALANCE_URL).hasAnyRole("USER", "ADMIN");
        });

        http.authorizeHttpRequests((requests) -> {
            requests.requestMatchers("/user").authenticated();
            requests.requestMatchers(NOTICE_URL, CONTACT_URL, "/register").permitAll();
        });
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }

//    @Bean
//    public InMemoryUserDetailsManager userDetailsManager() {

    /**
     * Approach 1: we use withDefaultPasswordEncoder() method while creating the user details.
     */
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("user1")
//                .password("password")
//                .roles("admin")
//                .build();
//
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user2")
//                .password("password")
//                .roles("read")
//                .build();
//
//        return new InMemoryUserDetailsManager(admin, user);

    /**
     * Approach 2: where we use withDefaultPasswordEncoder() method with creating the user details.
     */
//        UserDetails admin = User.withUsername("user1")
//                .password("password")
//                .roles("admin")
//                .build();
//
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user2")
//                .password("password")
//                .roles("read")
//                .build();
//
//        return new InMemoryUserDetailsManager(admin, user);
//    }
//    @Bean
//    public UserDetailsService userDetailsService(DataSource dataSource) {
//        return new JdbcUserDetailsManager(dataSource);
//    }

    /**
     * NoOpPasswordEncoder is not recommended for production usage.
     * Use only for non-prod.
     *
     * @return
     */
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
