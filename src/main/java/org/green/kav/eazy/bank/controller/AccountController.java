package org.green.kav.eazy.bank.controller;

import org.green.kav.eazy.bank.model.Accounts;
import org.green.kav.eazy.bank.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/myAccount")
    public Accounts getAccountDetails(int id) {
        Accounts accounts = accountRepository.findByCustomerId(id);
        if (accounts != null) return accounts;
        return null;
    }
}
